
(function (){
    'use strict';

    function alternateSizes (API_Service, $timeout) {
        return function (scope, element) {

            // Gets the sizes for the image with the filtername.
            var sizes = scope.vm.IMAGE_SIZE[scope.filterName];

            var image = (typeof scope.image.length == 'number') ? scope.image[0] : scope.image;

            if (!angular.isUndefined(image) && image != null) {

                var alternateSizes = (typeof image.alternate_sizes != 'undefined') ?
                    image.alternate_sizes : '';

                if (typeof scope.filterName == 'undefined') {
                    console.log('filtername not set for image ' + scope.attrs.alt);
                }

                if (typeof sizes == 'undefined') {
                    console.log('filtername sizes not found: ' + scope.filterName);
                }

                var objContainer = {};
                var count = 0;
                scope.imageLoader = [];

                // Evaluates the differents sizes of the config file.
                for (var prop in sizes) {

                    var id = (typeof image.length == 'undefined') ? image.id : image[0].id;
                    var found = false;
                    var obj = {};

                    // Sets an empty name for each image file
                    scope.imageLoader[prop] = '';

                    for (var i = 0; i < alternateSizes.length; i++) {

                        var height = alternateSizes[i].meta.height;
                        var width = alternateSizes[i].meta.width;
                        var name = scope.vm.PATH_CONFIG.locations['core'] + alternateSizes[i].file;

                        if (sizes[prop]['width'] == width && sizes[prop]['height'] == height) {

                            // Sets the correct image name if the image is found in the alternate sizes.
                            scope.imageLoader[prop] = name;

                            // Puts the variable to found image
                            found = true;
                        }
                    }

                    // If the image is not found
                    if (!found) {
                        obj.id = id;
                        obj.width = sizes[prop]['width'];
                        obj.height = sizes[prop]['height'];
                        obj.source = (!angular.isUndefined(scope.image[0])) ? scope.image[0].source : scope.image.source;

                        // Object to send is pushed to the objContainer array
                        objContainer[count] = obj;
                        count++;
                    }

                }

                if (count > 0) {

                    // Send the images sizes not found to be generated in the CMS
                    API_Service(host + 'api/images/resize', {}, true).save({}, objContainer,
                        function success(response) {

                            if (scope.image != false) {
                                if (typeof scope.image.length == 'number') {
                                    scope.image[0].alternate_sizes = [];
                                } else {
                                    scope.image.alternate_sizes = [];
                                }
                            }

                            for (var prop in sizes) {
                                for (var i = 0; i < response.length; i++) {
                                    if (response[i].meta.width == sizes[prop]['width'] && response[i].meta.height == sizes[prop]['height']) {
                                        scope.imageLoader[prop] = scope.vm.PATH_CONFIG.locations['core'] + response[i].file;

                                        if (scope.image != false) {
                                            if (typeof scope.image.length == 'number') {
                                                scope.image[0].alternate_sizes.push(response[i]);
                                            } else {
                                                scope.image.alternate_sizes.push(response[i]);
                                            }
                                        }

                                    }
                                }
                            }

                            $timeout(function () {
                                return scope.imageClass = 'lazyload';
                            }, 0);
                        },
                        function error(data) {
                            console.log('images not regenerated');
                        });

                } else {

                    $timeout(function () {
                        return scope.imageClass = 'lazyload';
                    }, 0);
                }
            }
        }
    }

    alternateSizes.$inject = ['API_Service', '$timeout'];

    angular
        .module('bravoureAngularApp')
        .factory('alternateSizes', alternateSizes);

})();



